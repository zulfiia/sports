import { combineReducers } from 'redux';

import sports from './sportsReducer';
import filters from './filterReducer';

export default combineReducers({
  filters,
  sports, 
});