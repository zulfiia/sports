//reducer для параметров фильтров

const initialState = {
};

export default function filterReducer(state = initialState, action, params) {
    switch(action.type) {
        case 'GET_FILTER': {
            return {
                ...state,
            }
        }
        case 'SET_FILTER': {
            return {
                ...action.params
            }
        }
        case 'RESET_FILTER': {
            return {
                ...initialState,
            }
        }        
         
        default: return state;
        
    }
};
