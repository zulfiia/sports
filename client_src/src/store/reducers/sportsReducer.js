//reducer для объекта sports

const initialState = {
    sports: [],
    isFetching: false,
    error: null
};

export default function sportReducer(state = initialState, action, params) {
    switch(action.type) {
        case 'FETCH_SPORTS': {
            return {
                ...state, isFetching: true
            }
        }
        case 'FETCH_SPORTS_SUCCESS': {
            return {
                ...state,
                sports: action.result,
                isFetching: false,
            }
        }
        case 'FETCH_SPORTS_FAIL': {
            return {
                ...state,
                isFetching: false,
                error: action.result
            }
        }  
        case 'GET_SPORTS': {
            return {
                ...state, isFetching: true
            }
        }
        case 'GET_SPORTS_SUCCESS': {
            return {
                ...state,
                sports: [action.result],
                isFetching: false,
            }
        }
        case 'GET_SPORTS_FAIL': {
            return {
                ...state,
                isFetching: false,
                error: action.result
            }
        }          
        case 'ADD_SPORT': {
            return {
                ...state, 
                isFetching: true
            }
        }
        case 'ADD_SPORT_SUCCESS': {
            const newSport = state.sports;
            newSport.push(action.result);
            return {
                ...state,
                sports: newSport,
                isFetching: false,
            }
        }
        case 'ADD_SPORT_FAIL': {
            return {
                ...state,
                isFetching: false,
                error: action.result
            }
        } 
        case 'DELETE_SPORT': {
            return {
                ...state, 
                isFetching: true
            }
        }
        case 'DELETE_SPORT_SUCCESS': {
            const deletedItemId = action.params.id;
            const deletedItemIndex = state.sports.findIndex((item, index) => item.id === deletedItemId);
            const newSports = state.sports.slice(0);
            if(deletedItemIndex || deletedItemIndex === 0) {
                newSports.splice(deletedItemIndex, 1);
            }
            return {
                ...state,
                sports: newSports,
                isFetching: false,
            }
        }
        case 'DELETE_SPORT_FAIL': {
            return {
                ...state,
                isFetching: false,
                error: action.result
            }
        } 
        case 'CHANGE_SPORT': {
            return {
                ...state, 
                isFetching: true
            }
        }
        case 'CHANGE_SPORT_SUCCESS': {
            const changedItemId = action.params.id;
            const changedItemIndex = state.sports.findIndex((item, index) => item.id === changedItemId);
            const newSports = state.sports.slice(0);
            if(changedItemIndex || changedItemIndex === 0) {
                newSports[changedItemIndex] = action.result;
            }
            return {
                ...state,
                sports: newSports,
                isFetching: false,
            }
        }
        case 'CHANGE_SPORT_FAIL': {
            return {
                ...state,
                isFetching: false,
                error: action.result
            }
        }         
        default: return state;
    }
};
