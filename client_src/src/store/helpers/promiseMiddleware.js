//обработка promise из action
//здесь обрабатывается запрос, его результат.

export default function promiseMiddleware() {
  return next => action => {
    
    const { promise, type, ...others } = action;
  
      //если это не запрос, то направляем action в reducer
      if (!promise) return next(action);
  
      
      const REQUEST = type;
      const SUCCESS = type + '_SUCCESS';
      const FAIL = type + '_FAIL';
      next({ ...others, type: REQUEST });
  
      return promise
        .then(res => {
          next({ 
            ...others, 
            result: res.data, 
            type: SUCCESS 
          });
  
          return true;
        })
        .catch(error => {
          next({ 
            ...others, 
            error: error, 
            type: FAIL
          });
          console.log('error =>', error);
  
          return false;
        });
    };
  }