import axios from 'axios';

export function getSports() {
  return {
    type: 'FETCH_SPORTS',
    promise: axios.get('http://localhost:3000/api/sports')
  }
} 

export function getSportId(id) {
  return {
    type: 'GET_SPORTS',
    params: id,
    promise: axios.get(`http://localhost:3000/api/sports/${id}`)
  }
} 

export function addSport(params) {
  return {
    type: 'ADD_SPORT',
    promise: axios.post('http://localhost:3000/api/sports', {...params})
  }
}  

export function editSport(params) {
  return {
    type: 'CHANGE_SPORT',
    params,
    promise: axios.put('http://localhost:3000/api/sports', {...params})
  }
}  

export function deleteSport(params) {
  return {
    type: 'DELETE_SPORT',
    params,
    promise: axios.delete(`http://localhost:3000/api/sports/${params.id}`)
  }
}  