export function getFilter() {
  return {
    type: 'GET_FILTER',
  }
} 

export function setFilter(params) {
    return {
      type: 'SET_FILTER',
      params
    }
} 

export function resetFilter() {
    return {
      type: 'RESET_FILTER',
    }
} 