import { createSelector } from 'reselect';
import { getFilters } from './filtersSelector';

export const getSports = (state) => state && state.default && state.default.sports;

// const compareWithFilters = (filters, item) => {
//     const filtersArr = Object.keys(filters);
//     for(let i = 0; i < filtersArr.length; i++) {
//         const prop = filtersArr[i];
//         if(prop === 'isOlympic') {
//             if(!(item.year > 0)){
//                 return false;
//             }
//         } 
//         if (prop === 'q') {
//             if(!item.name.toLowerCase().includes(filters.q)) {
//                 return false
//             }
//         }
//         if(item[prop] !== filters[prop]) {
//             return false;
//         }
//         return true;
//     }
// };


//Селектор получения отфильтрованного массива спорта
export const getFilteredSports = createSelector(
    [ getSports, getFilters ],
    (sports, filters) => {
        const sportsArr = sports.sports || [];
        if(Object.keys(filters).length === 0) {
            return sportsArr;
        }

        // const final = sportsArr.filter((sport) => {
        //     return compareWithFilters(filters, sport);
        // }); 

        const filteredSports = sportsArr.reduce((obj, item) => {
            if(filters.q && !(item.name.toLowerCase().includes(filters.q))) {
                return obj;
            }
            if(filters.isOlympic && !(item.year > 0)) {
                return obj;
            }
            if(filters.isSummer && !(item.isSummer)) {
                return obj;
            } 
            if(filters.isSummer === false && !(item.isSummer === false)) {
                return obj;
            }    
            if(filters.isTraumatic && !item.isTraumatic) {
                return obj;
            }    
            if(filters.isWater && !item.isWater) {
                return obj;
            }   
            if(filters.isCommand && !item.isCommand) {
                return obj;
            }   
            if(filters.isCommand === false && item.isCommand) {
                return obj;
            }    
            
            obj.push(item);
            return obj;

        }, []); 

        return filteredSports;

    }
);