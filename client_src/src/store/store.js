import { applyMiddleware, createStore, combineReducers } from 'redux';
import promiseMiddleware from './helpers/promiseMiddleware';
import * as reducers  from './reducers';

const reducer  = combineReducers(reducers);
export const store = applyMiddleware(promiseMiddleware)(createStore)(reducer);
