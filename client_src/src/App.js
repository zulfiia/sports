import React, { Component } from 'react';
import {Provider} from 'react-redux';

import { store } from './store/store.js'; 

import Header from './Header';
import Main from './Main';

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
      <Provider store={store} key='provider'>
        <div>
          <Header/>
          <Main/>
        </div>
      </Provider>
    );
  }
}

export default App;
