import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

import './Header.css';

export default class Header extends Component {
  goCreate = () => {
    if(this.context.router)
    this.context.router.history.push(`/new`);   
  }

  goHome = () => {
    if(this.context.router)
    this.context.router.history.push(`/`);   
  }  

  render() {
    return (
      <div className='header-container'>

          <div className='header-container__logo' onClick={() => this.goHome()}>
            Pro-cпорт
          </div>

          <div className='header-container__button-wrapper'>
            <Button color="primary" outline onClick={() => this.goCreate()}>
                + Добавить
            </Button>
          </div>
      </div>
    );
  }
}

Header.contextTypes = {
  router: PropTypes.object.isRequired,
};