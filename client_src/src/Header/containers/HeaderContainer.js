import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getSports } from '../../store/actions/sportsActions';
import { getFilter } from '../../store/actions/filterActions';

import Header from '../components/Header';

class HeaderContainer extends Component {
    componentDidMount() {
        this.props.getSports();
    }

    render() {
        return (
            <Header />
        );
    }
}

const mapStateToProps = (state, param) => {
    return {
        ...state,
    };
  };
  
  const mapActionCreators = {
    getSports,
    getFilter
  };

  export default connect(mapStateToProps, mapActionCreators)(HeaderContainer);