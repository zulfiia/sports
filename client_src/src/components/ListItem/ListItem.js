import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { 
  Card, 
  CardImg, 
  CardText, 
  CardBody,
  CardTitle, 
  CardSubtitle, 
  Badge  
} from 'reactstrap';

import water from '../../img/water.svg'
import traumatic from '../../img/traumatic.svg';
import summer from '../../img/summer.svg';
import winter from '../../img/winter.svg';
import solo from '../../img/solo.svg';
import team from '../../img/team.svg';

import './ListItem.css';

export default class ListItem extends Component {
  edit = id => {
    if(this.context.router)
    this.context.router.history.push(`/edit/${id}`);
  }
  render() {
    const { item, deleteSport } = this.props;

    return (
      <div className='card-wrapper'>
        <Card>
          <CardImg 
            width="28%" 
            src={
              item.image 
                ? `http://localhost:3000/api/containers/sportsImg/download/${item.image}` 
                : 'http://www.nokiaplanet.com/uploads/posts/2013-01/1357122565_ted-picture-480x800.jpg'
            }
            alt="Card image cap" 
          />
          <CardBody>
            <div className='flexBlock title-wrapper'>
              <CardTitle>{item.name}</CardTitle>
              <div className='flexBlock card__button-block'>
                <Badge 
                  onClick={() => this.edit(item.id)} 
                  color="primary"
                >
                    Изменить
                </Badge >{' '}
                <Badge  
                  color='secondary' 
                  onClick={() => deleteSport(item)}
                >
                  Удалить
                </Badge >
            </div>              
            </div>
            <CardSubtitle>
                {item.year 
                  ? `В составе олимпийских игр с ${item.year}`
                  : 'Не входит в состав олимпийских игр'
                }
            </CardSubtitle>
            <div className='flexBlock card__info-block'>
              <div className='flexBlock card__info-item'>
                <img 
                  alt='травмоопасность'
                  className='card__info-icon' 
                  src={traumatic}
                  style={{opacity: item.isTraumatic ? 1 : 0.4}}
                />
                <span>
                  <span className='no-wrap-text'>
                    {item.isTraumatic ? 'Травмоопасный ' : 'Не травмоопасный'} 
                  </span>
                  <br /> 
                  спорт
                </span>
              </div>
              <div className='flexBlock card__info-item'>
                  <img 
                    alt={item.isSummer ? 'летний' : 'зимний'}
                    className='card__info-icon' 
                    src={item.isSummer ? summer : winter} 
                  />
                  <span>
                    <span className='no-wrap-text'>
                      {item.isSummer ? 'Летний ' : 'Зимний '}
                    </span>
                    <br />
                    вид
                  </span>
              </div>
              <div className='flexBlock card__info-item'>
                  <img 
                    alt='водный'
                    className='card__info-icon' 
                    src={water}
                    style={{opacity: item.isWater ? 1 : 0.4}}
                  />
                  <span>
                    <span className='no-wrap-text'>
                      {item.isWater ? 'Водный ' : 'Не водный '}
                    </span>
                    <br /> спорт
                  </span>
              </div>
              <div className='flexBlock card__info-item'>
                  <img 
                    alt={item.isCommand ? 'командный' : 'индивидуальный'}
                    className='card__info-icon' 
                    src={item.isCommand ? team : solo}
                  />
                  <span>
                    <span className='no-wrap-text'>
                      {item.isCommand ? 'Командный ' : 'Не командный '}
                    </span>
                    <br /> спорт
                  </span>
              </div>
              
            </div>
            <CardText>
              {
                `Является ${item.isCommand ? 'командным спортом' : 'не командным спортом'}.
                ${item.isSummer ? 'Летний ' : 'Зимний '} спорт.
                Данный вид является ${item.isTraumatic ? 'весьма' : 'не'} травмоопасным.
                Это ${item.isWater ? '' : 'не'} водный спорт.`
              }
            </CardText>
          </CardBody>
        </Card>
      </div>
    );
  }
}

ListItem.contextTypes = {
  router: PropTypes.object.isRequired,
};

