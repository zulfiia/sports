import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { 
    Form, 
    FormGroup, 
    Label, 
    Input, 
} from 'reactstrap';

import './Panel.css';

export default class Panel extends Component {
  setFilterParams = (prop, value) => {
    const newFilters = {...this.props.filters};
    newFilters[prop] = value;
    this.props.setFilter({...JSON.parse(JSON.stringify(newFilters))});
  }

  render() {
    const { filters } = this.props;
    const { setFilterParams } = this;
    return (
      <div className='card-wrapper'>
        <Form>
          <FormGroup>
            <Input 
                type="text" 
                id="searchItem" 
                placeholder="Введите название" 
                value={filters.q}
                onChange={e => setFilterParams('q', e.target.value === '' ? undefined : e.target.value)}
            />
          </FormGroup>

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isOlympic}
              type="checkbox" 
              onClick={e => setFilterParams('isOlympic', filters.isOlympic ? undefined : true)}           
            />{' '}
              Олимпийский
            </Label>
          </FormGroup>

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isSummer}
              type="checkbox" 
              onClick={e => setFilterParams('isSummer', filters.isSummer ? undefined : true)}
            />{' '}
              Летний
            </Label>
          </FormGroup> 

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isSummer === false}
              type="checkbox" 
              onClick={e => setFilterParams('isSummer', filters.isSummer === false ? undefined : false)}           
            />{' '}
              Зимний
            </Label>
          </FormGroup>                      

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isTraumatic}
              type="checkbox" 
              onClick={e => setFilterParams('isTraumatic', filters.isTraumatic ? undefined : true)}
            />{' '}
              Травмоопасный
            </Label>
          </FormGroup> 

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isWater}
              type="checkbox" 
              onClick={e => setFilterParams('isWater', filters.isWater ? undefined : true)}
            />{' '}
              Водный
            </Label>
          </FormGroup>  

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isCommand}
              type="checkbox" 
              onClick={e => setFilterParams('isCommand', filters.isCommand ? undefined : true)}
            />{' '}
              Командный
            </Label>
          </FormGroup> 

          <FormGroup check>
            <Label check>
            <Input 
              checked={filters.isCommand === false}
              type="checkbox" 
              onClick={e => setFilterParams('isCommand', filters.isCommand === false ? undefined : false)}
            />{' '}
              Индивидуальный
            </Label>
          </FormGroup> 
        </Form>
      </div>
    );
  }
}

Panel.contextTypes = {
  router: PropTypes.object.isRequired,
};

