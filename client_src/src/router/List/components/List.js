import React, { Component } from 'react';

import ListItem from '../../../components/ListItem';
import Panel from '../../../components/Panel';

import './List.css';

class List extends Component {
  render() {
    const { 
      sports, 
      history, 
      deleteSport,
      filters,
      setFilter,
      resetFilter 
    } = this.props;
    return (
      <div className='wrapper-list'>
        <div>
          <Panel 
            filters={filters}
            setFilter={setFilter}
            resetFilter={resetFilter}
          />
        </div>  

        <div className='wrapper-list__list'>
          {sports && sports.map(item => (
            <div key={item.id} className=''>
              <ListItem 
                item={item} 
                deleteSport={deleteSport} 
                history={history}
              />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default List;
