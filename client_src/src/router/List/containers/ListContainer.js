import { connect } from 'react-redux';

import { getSports, deleteSport } from '../../../store/actions/sportsActions';
import { getFilter, setFilter, resetFilter } from '../../../store/actions/filterActions';
import { getFilteredSports } from '../../../store/selectors/sportsSelector';
import { getFilters } from '../../../store/selectors/filtersSelector';

import List from '../components/List';

const mapStateToProps = (state, param) => {
  const sports = getFilteredSports(state);
  const filters = getFilters(state);
    return {
        ...state,
        sports, 
        filters
    };
  };
  
  const mapActionCreators = {
    getSports,
    deleteSport,
    getFilter,
    setFilter,
    resetFilter
  };

  export default connect(mapStateToProps, mapActionCreators)(List);