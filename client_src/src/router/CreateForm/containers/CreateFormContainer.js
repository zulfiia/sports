import { connect } from 'react-redux';

import { addSport, editSport } from '../../../store/actions/sportsActions';

import CreateForm from '../components/CreateForm';

const initialItemModel = {
  name: '',
  isCommand: false,
  isTraumatic: false,
  year: 2018,
  isSummer: false,
  isWater: false,
  image: ''
}

const mapStateToProps = (state, { match, location }) => {
    const id = match.params.id;
    const sports = state.default.sports.sports;
    let item;
    if(id && sports.length > 0) {
      item = sports.reduce((obj, item) => {
          if(item.id === id) {
            obj = {...item};
          }
          return obj;
        }, {}
      );   
    } else {
      item = {...initialItemModel}
    }
    return {
        id,
        item,
        location,
        sports: sports
    };
  };
  
  const mapActionCreators = {
    addSport,
    editSport
  };

  export default connect(mapStateToProps, mapActionCreators)(CreateForm);