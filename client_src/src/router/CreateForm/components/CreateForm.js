import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { 
  Button, 
  Form, 
  FormGroup, 
  Label, 
  Input, 
  Alert 
} from 'reactstrap';
import Dropzone from 'react-dropzone';

import './CreateForm.css';

export default class CreateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {...props.item},
      errorText: false
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.item.id !== this.props.item.id) {
      this.setState({item: {...nextProps.item}});
    }
  }

  S4 = () => {
    return (((1+Math.random())*0x10000)|0).toString(16).substring(1); 
  }

  generateGuid = () => {
    const { S4 } = this;
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0,3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();  
  }

  onSubmit = () => {
    if(this.state.item.name) {
      if(this.state.item.id) {
        this.props.editSport({...this.state.item});
      } else {
        this.props.addSport({...this.state.item});
      }
      
      if(this.state.item.image && this.state.formData) {
        axios('http://localhost:3000/api/containers/sportsImg/upload', {
          method: 'POST',
          responseType: 'blob',
          data: this.state.formData
        }).then(res => console.log('image uploaded'));    
      }
      this.goBack();
    }

    this.setState({errorText: true});
  }

  goBack = () => {
    this.context.router.history.goBack();
  }

  dataURItoBlob = dataURL => {
    const byteString = atob(dataURL.split(',')[1]);
    const mimeString = dataURL.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ab], {type: mimeString});
    return blob;
  }  

  drop = (image, prop, e ) => {
    const extention = image[0].name.split('.');
    const nameFile = this.generateGuid() + '.' + extention[extention.length - 1];

    const formData = new FormData();
    formData.append("file", image[0], nameFile);
    this.setState({
      item: {
        ...this.state.item,
        image: nameFile,
      },
      formData
    });   
  }  

  render() {
    const { item, errorText } = this.state;
    return (
      <div className='create-form'>
        <Form>
          <FormGroup>
            <Label for="nameItem">Название *</Label>
            <Input 
              type="text" 
              name="name" 
              id="nameItem" 
              placeholder="Введите название" 
              value={item.name} 
              onChange={e => this.setState({item: {...item, name: e.target.value}})}
            />
          </FormGroup>

          <FormGroup>
            <Label for="yearInclude">
              Год включения в программу Олимпийских игр
            </Label>
            <Input 
              type="number" 
              name="year" 
              id="yearInclude" 
              placeholder="Введите год" 
              value={item.year} 
              onChange={e => this.setState({item: {...item, year: e.target.value}})}
            />
          </FormGroup>

          <div className="file-zone">
            <Dropzone
              style={{
                height: '200px',
                width: 'auto'
              }}
              onDrop={this.drop}
              multiple={false}
              accept='image/*'
              disableClick={false}
            > 
                {this.state.formData && this.state.item.image 
                ? (
                  <div className='file-zone__text'>
                    Файл загружен,<br /> если хотите загрузить новый, перетащите или нажмите
                  </div>
                ) : (
                  <div className='file-zone__text'>
                    Перетащите или нажмите, чтобы загрузить файлы
                  </div>
                )
              }
            </Dropzone>
          </div>     

          <FormGroup tag="fieldset">
            <legend>Сезон спорта</legend>
            
            <FormGroup check>
              <Label check>
                <Input 
                  checked={item.isSummer === false} 
                  type="radio" 
                  name="winter"
                  onClick={e => 
                    this.setState({
                      item: {
                        ...item, 
                        isSummer: false
                      }
                    })
                  }
                />{' '}
                Зимний
              </Label>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input 
                  checked={item.isSummer} 
                  type="radio" 
                  name="summer" 
                  onClick={e => 
                    this.setState({
                      item: {
                        ...item, 
                        isSummer: true
                      }
                    })
                  }            
                />{' '}
                Летний
              </Label>
            </FormGroup>
          </FormGroup>

          <FormGroup check>
            <Label check>
              <Input 
                checked={item.isTraumatic} 
                type="checkbox" 
                onClick={e => 
                    this.setState({
                      item: {
                        ...item, 
                        isTraumatic: !item.isTraumatic
                      }
                    })
                  }          
              />{' '}
              Травмоопасный спорт
            </Label>
          </FormGroup>

          <FormGroup check>
            <Label check>
              <Input 
                checked={item.isWater} 
                type="checkbox" 
                onClick={e =>{ 
                    this.setState({
                      item: {
                        ...item, 
                        isWater: !item.isWater
                      }
                    })}
                  }           
              />{' '}
              Водный спорт
            </Label>
          </FormGroup> 

          <FormGroup check>
            <Label check>
              <Input 
                checked={item.isCommand} 
                type="checkbox" 
                onClick={e => 
                    this.setState({
                      item: {
                        ...item, 
                        isCommand: !item.isCommand
                      }
                    })
                  }           
              />{' '}
              Командный спорт
            </Label>
          </FormGroup>     
          
          <div className='flexBlock'>
            <div>
              <Button onClick={() => this.onSubmit()}>Сохранить</Button>
              <Button onClick={() => this.setState({item: {...this.props.item}})}>Отменить</Button>
            </div>
            {errorText && (
              <div className='alert-wrapper'>
                <Alert color="danger">
                  Заполните поле "Название"
                </Alert>
              </div>            
            )}
          </div>
        </Form>
      </div>
    );
  }
}

CreateForm.contextTypes = {
  router: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};
