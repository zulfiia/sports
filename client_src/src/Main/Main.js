import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import List from '../router/List';
import CreateForm from '../router/CreateForm';

import './Main.css';

class Main extends Component {
  render() {
    return (
      <div className='main-container'>
        <Switch>
          <Route exact path='/' component={List}/>
          <Route path='/new' component={CreateForm}/>
          <Route path='/edit/:id' component={CreateForm}/>
        </Switch>        
      </div>
    );
  }
}

export default Main;
